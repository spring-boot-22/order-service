# set of instructions to build a Docker image

# Base OS on top which the image will be baked
FROM openjdk:8-alpine as build

# Add the Maintainers
LABEL maintainer="Pradeep Kumar L"

ARG JAR_FILE

COPY ${JAR_FILE} app.jar

RUN mkdir -p target/dependency && (cd target/dependency; jar -xf /app.jar)


# Second stage

FROM openjdk:8-alpine

# Copy all the unpackaged application
ARG DEPENDENCY=/target/dependency

COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app

#execute
ENTRYPOINT ["java", "-cp", "app:app/lib/*", "com.classpath.orderservice.OrderServiceApplication"]

