package com.classpath.orderservice.exception;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    public Error handleRuntimeExceptions(RuntimeException exception){
      log.error(" Came inside the exception handler ");
      return Error.builder().errorCode(12).message(exception.getMessage()).build();
    }
}

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter @Getter
class Error {
    private long errorCode;
    private String message;
}