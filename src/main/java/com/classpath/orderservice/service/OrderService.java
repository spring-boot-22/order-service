package com.classpath.orderservice.service;

import com.classpath.orderservice.model.Order;

import java.util.Set;

public interface OrderService {

    Order save(Order order);

    Set<Order> fetchOrders();

    Order fetchOrderById(long orderId);

    void deleteOrderById(long orderId);
}