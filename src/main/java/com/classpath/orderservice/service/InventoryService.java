package com.classpath.orderservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class InventoryService {

    private static int inventoryCount = 100;

    public void updateInventory(){
        --inventoryCount;
      log.info("Update the inventory  :: "+ inventoryCount);
    }

}