package com.classpath.orderservice.service;

import com.classpath.orderservice.model.Order;
import com.classpath.orderservice.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final InventoryService inventoryService;

    @Override
    public Order save(Order order) {
        Order savedOrder = this.orderRepository.save(order);
        inventoryService.updateInventory();
        return savedOrder;
    }

    @Override
    public Set<Order> fetchOrders() {
        return new HashSet<>(this.orderRepository.findAll());
    }

    @Override
    public Order fetchOrderById(long orderId) {
        return this.orderRepository
                .findById(orderId)
                .orElseThrow(()-> new IllegalArgumentException("Invalid Order Id"));
    }

    @Override
    public void deleteOrderById(long orderId) {
        this.orderRepository.deleteById(orderId);
    }
}