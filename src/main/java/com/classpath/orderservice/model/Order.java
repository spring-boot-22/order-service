package com.classpath.orderservice.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

import static javax.persistence.GenerationType.AUTO;

@Setter
@Getter
@ToString
@Builder
@EqualsAndHashCode (of = "orderId")
@Entity
@Table(name = "orders")
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    @NotNull(message = "created date cannot be null")
    private LocalDate createdDate;

    @Min(value = 1000, message = "The order value should be minimum of 1000")
    private double orderPrice;

    @NotEmpty(message = "customer name cannot be empty")
    private String customerName;

}