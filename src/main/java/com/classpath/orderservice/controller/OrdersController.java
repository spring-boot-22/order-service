package com.classpath.orderservice.controller;

import com.classpath.orderservice.model.Order;
import com.classpath.orderservice.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
@Slf4j
public class OrdersController {

    private final OrderService orderService;

    @GetMapping
    public Set<Order> fetchOrders(){
      log.info("Inside the fetchOrders method ");
      return this.orderService.fetchOrders();
    }

    @GetMapping("/{orderId}")
    public Order fetchOrderById(@PathVariable long orderId){
      log.info("Inside the fetchOrder by Id method ");
      return this.orderService.fetchOrderById(orderId);
    }

    @PostMapping
    public Order saveOrder(@RequestBody @Valid Order order){
        log.info("Saving the order in the db ::");
        return this.orderService.save(order);
    }
}